import React from "react"
import { Platform } from "react-native"
import { createStackNavigator, createBottomTabNavigator } from "react-navigation"

import TabBarIcon from "../components/TabBarIcon"
import HomeScreen from "../screens/HomeScreen"
import LinksScreen from "../screens/LinksScreen"
import SettingsScreen from "../screens/SettingsScreen"
import BuscarScreen from "../screens/BuscarScreen"

const HomeStack = createStackNavigator({
	Home: HomeScreen,
})

HomeStack.navigationOptions = {
	tabBarLabel: "Home",
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={
				Platform.OS === "ios"
					? `ios-information-circle${focused ? "" : "-outline"}`
					: "md-information-circle"
			}
		/>
	),
}

const BuscarStack = createStackNavigator({
	Links: BuscarScreen,
})

BuscarStack.navigationOptions = {
	tabBarLabel: "Buscar",
	tabBarIcon: ({ focused }) => ( 
		<TabBarIcon 
			focused = {focused}
			name = {Platform.OS === "ios" ? `ios-search${focused ? "" : "-outline"}` : "md-search"} 
		/>
	),
}


const LinksStack = createStackNavigator({
	Links: LinksScreen,
})

LinksStack.navigationOptions = {
	tabBarLabel: "Links",
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={Platform.OS === "ios" ? `ios-link${focused ? "" : "-outline"}` : "md-link"}
		/>
	),
}

const SettingsStack = createStackNavigator({
	Settings: SettingsScreen,
})

SettingsStack.navigationOptions = {
	tabBarLabel: "Settings",
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={Platform.OS === "ios" ? `ios-options${focused ? "" : "-outline"}` : "md-options"}
		/>
	),
}

export default createBottomTabNavigator({
	HomeStack,
	BuscarStack,
	LinksStack,
	SettingsStack
})
