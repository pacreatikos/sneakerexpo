import React from "react"

import  {
	Text,
	View,
	StyleSheet,
	Platform,
	Image,
	ScrollView,
	TouchableHighlight,
	WebView
} from "react-native"

import { WebBrowser } from "expo"

import { Grid, Col, Row } from 'react-native-easy-grid'

export default class Ficha extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			busqueda: ""
		  };
	}

	_crearUrl(){
		/* TODO: CREAR URL DESDE PROPS*/ 
	}

	_handlePressBump = () => {
		this.props.setModalInvisible()
		WebBrowser.openBrowserAsync("https://sobump.com/")
	}
	
	_handlePressSole = async () => {
		const urlSole = "https://thesolesupplier.co.uk/release-dates/?search_key=" + `${this.props.datos.brand} ${this.props.datos.name}`
		const urlSoleOk = urlSole.split(' ').join('+').toString()
		this.props.setModalInvisible()
		await WebBrowser.openBrowserAsync(urlSoleOk)
	}

	_handlePressGoat = () => {
		const urlGoat = "https://www.goat.com/search?query=" + `${this.props.datos.brand} ${this.props.datos.name}`
		const urlGoatOk = urlGoat.split(' ').join('+').toString()
		this.props.setModalInvisible()
		WebBrowser.openBrowserAsync(urlGoatOk)
	}

	_handlePressStockx = () => {
		const urlStock = "https://stockx.com/search?s=" + `${this.props.datos.brand} ${this.props.datos.name}`
		const urlStockOk = urlStock.split(' ').join('+').toString()
		this.props.setModalInvisible()
		WebBrowser.openBrowserAsync(urlStockOk)
	}
  
	render() {
		return (
 			<ScrollView style={styles.scroll}>
				<View style={styles.centrado}>
					<Grid>
						<Row style={styles.rowEstiloImagen}>
							<Image style={styles.imagen} source={{uri: this.props.datos.imageUrl}} />
						</Row>
						<Row style={styles.rowEstilo}>
							<Text style={styles.titulo} numberOfLines={1}>{this.props.datos.brand} {this.props.datos.name}</Text>
						</Row>
						<Row style={styles.rowEstilo}>
							<Text style={styles.titulo} numberOfLines={1}>Fecha de salida: {this.props.datos.releaseDate}</Text>
						</Row>
						<Row style={styles.rowEstilo}>
							<Text style={styles.titulo} numberOfLines={1}>Diseñador: {this.props.datos.shoe}</Text>
						</Row>
						<Row style={styles.rowEstilo}>
							<Text style={styles.titulo} numberOfLines={1}>Peor precio: {this.props.datos.annualLow} Mejor precio: {this.props.datos.annualHigh}</Text>
						</Row>
						<Row style={styles.rowEstilo}>
							<Text numberOfLines={1} style={styles.titulo}>COMPRAR EN:</Text>
						</Row>
						<Row style={styles.rowEstilo}>
							<Col size={50} style={styles.rowEstilo}>
								<TouchableHighlight 
										style={styles.option}
										onPress={this._handlePressSole}>
									<View>
										<View style={styles.optionIconContainer}>
											<Image style={styles.logo} source={require("../assets/images/sole.png")}></Image>
										</View>
									</View>
								</TouchableHighlight>
							</Col>
							<Col size={50} style={styles.rowEstilo}>
								<TouchableHighlight 
									style={styles.option}
									onPress={this._handlePressGoat}>
									<View>
										<View style={styles.optionIconContainer}>
											<Image style={styles.logo} source={require("../assets/images/goat.png")} />
										</View>
									</View>
									</TouchableHighlight>
							</Col>
						</Row>
						<Text>{"\n"}</Text>
						<Row>
							<Col size={50} style={styles.rowEstilo}>
								<TouchableHighlight 
									style={styles.option}
									onPress={this._handlePressBump}>
									<View>
										<View style={styles.optionIconContainer}>
											<Image style={styles.logo} source={require("../assets/images/bump.png")} />
										</View>
									</View>
								</TouchableHighlight>
							</Col>
							<Col size={50} style={styles.rowEstilo}>
								<TouchableHighlight 
									style={styles.option}
									onPress={this._handlePressStockx}>
									<View>
										<View style={styles.optionIconContainer}>
											<Image style={styles.logo} source={require("../assets/images/stockx.png")} />
										</View>
									</View>
								</TouchableHighlight>
							</Col>
						</Row>
					</Grid>
				</View>
			</ScrollView>
		)
	}
}



const styles = StyleSheet.create({
	imagen: {
		width: 300,
		height: 300,
		resizeMode: "contain",
		alignItems: "center"
	},logo: {
		width: 100,
		height: 100,
		resizeMode: "contain",
		alignItems: "center"
	},
	tabBarInfoContainer: {
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0,
		...Platform.select({
			ios: {
				shadowColor: "black",
				shadowOffset: { height: -3 },
				shadowOpacity: 0.1,
				shadowRadius: 3,
			},
			android: {
				elevation: 20,
			},
		}),
		alignItems: "center",
		backgroundColor: "#fbfbfb",
		paddingVertical: 20,
	},
	tabBarInfoText: {
		fontSize: 17,
		color: "rgba(96,100,109, 1)",
		textAlign: "center",
	},
	titulo: {
		fontSize: 20,
		alignItems: "center",
		justifyContent: "center"
	},
	centrado: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center"
	},
	rowEstilo: {
		height: 75,
		width: "100%",
		alignItems: "center",
		justifyContent: "center"
	},
	rowEstiloImagen: {
		paddingTop: 50,
		paddingBottom: 30,
		height: 300,
		width: "100%",
		alignItems: "center",
		justifyContent: "center"
	},
	scroll: {
		flex: 1,
		padding: 20
	}
})