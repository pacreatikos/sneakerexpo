import firebase from 'firebase'
require('firebase/firestore')

/* 
TODO: cambiar BBDD según las necesidades del momento

firebase oficial

const config = {
  apiKey: "AIzaSyDT8f7mCWPEx3be6p4P_XPWKvLOWwGNmu4",
  authDomain: "stock-e90eb.firebaseapp.com",
  databaseURL: "https://stock-e90eb.firebaseio.com",
  projectId: "stock-e90eb",
  storageBucket: "stock-e90eb.appspot.com",
  messagingSenderId: "711405912128"
} */

const config = {
  apiKey: "AIzaSyDvefuVYo2ZXntrrB4StzmrFzGCdIj-fH0",
  authDomain: "stockboilerplatejulio.firebaseapp.com",
  databaseURL: "https://stockboilerplatejulio.firebaseio.com",
  projectId: "stockboilerplatejulio",
  storageBucket: "stockboilerplatejulio.appspot.com",
  messagingSenderId: "403234348449"
};

firebase.initializeApp(config)
const firestore = firebase.firestore()

const settings = {
  timestampsInSnapshots: true
}

firestore.settings(settings)

export const db = firebase.firestore()
