import React from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
  View,
  FlatList,
  Modal,
  TouchableOpacity
} from 'react-native';

import { SearchBar, List, ListItem, Icon } from "react-native-elements"

import Ficha from '../components/Ficha'

import Mock from '../mock'

export default class BuscarScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: Mock,
      dataBackup: [],
      page: 1,
      seed: 1,
      error: null,
      refreshing: false,
      buscando: true,
      mostrar: "",
      modalVisible: false
    };
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    this.setState({ loading: true });
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: Mock,
          error: res.error || null,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false, refreshing: false });
      });
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  
  renderHeader = () => {
    return <SearchBar 
      placeholder="¿Que buscas?..." 
      lightTheme 
      round 
      maxLength={100}
      selectTextOnFocus={true}
      onChangeText={this.buscar}
      onClearText={this.borrarBusqueda}
      clearTextOnFocus={true}
      clearButtonMode={'always'}
      keyboardAppearance={'dark'}
      />
  }

  renderFooter = () => {
    if (!this.state.loading) return null;

    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };

  buscar = (busqueda) => {

    console.log("Buscando: " + busqueda)

    this.setState({
      buscando: true
    })

    datos = Mock
    buscate = busqueda.trim().toLowerCase()

    datos = datos.filter(l => {
        return l.name.toLowerCase().match( buscate ) || l.brand.toLowerCase().match( buscate ) ||l.shoe.toLowerCase().match( buscate )
      }
    )

    this.setState({
      data: datos
    })

    console.log("datos finales:")
    console.log(this.state.data)
  }

  borrarBusqueda = () => {

    console.log("borrar busqueda")

    this.setState({
      data: Mock,
      buscando: false
    })
  }

  handleRefresh = () => {
    if(this.state.buscando == false){
      this.setState({
        page: 1,
        refreshing: true,
        seed: this.state.seed + 1
      }, () => {
        this.makeRemoteRequest()
      })
    }
  }

  handleLoadMore = () => {
    if(this.state.buscando == false){
      this.setState({
        page: this.state.page + 1,
      }, () => {
        this.makeRemoteRequest()
      })
    }
  }

  setModalVisible(item){
    this.setState({ 
      mostrar: item,
      modalVisible: true
    })
  }

  setModalInvisible(){
    this.setState({ 
      modalVisible: false
    })
  }

  render() {
    return (
      <View>
          <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              selectedItem={this.state.selectedItem}
              onRequestClose={ () => {this.setModalInvisible()} }
            >
              <TouchableOpacity
                onPress={() => {this.setModalInvisible()}}
                style={styles.modalBackIcon}
                >
                  <Icon style={styles.modalBackIcon} type='ionicon' name="ios-arrow-back" />
              </TouchableOpacity>
                <Ficha datos={this.state.mostrar} setModalInvisible={() => this.setModalInvisible()} />
            </Modal>
        <List
          containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}
        >
          <FlatList
            keyboardShouldPersistTaps ='always' 
            keyboardDismissMode='on-drag'
            data={this.state.data}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={this.renderSeparator}
            ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0}
            renderItem={({ item }) => (
            <ListItem
              title={`${item.brand} ${item.shoe} ${item.name}`}
              subtitle={item.annualLow}
              avatar={{ 
                uri: item.smallImageUrl,
                avatarStyle: styles.avatar
              }}
              containerStyle={{ borderBottomWidth: 0 }}
              onPress={() => {this.setModalVisible(item)}}
            />
            )}
          />
        </List>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  textoEnorme: {
    fontSize: 40,
    alignSelf: 'center',
  },
  avatar: {
    resizeMode: "cover"
  },
	modalBackIcon: {
    paddingLeft: 30,
    paddingTop: 30,
    alignItems: "flex-start"
	}
});
