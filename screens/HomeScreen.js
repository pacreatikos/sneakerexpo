import React from 'react';
import {
  Platform,
  StyleSheet,
  FlatList,
  View,
  Image,
  Dimensions,
  Text,
  Modal,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';

import {Icon} from 'react-native-elements'

import Mock from '../mock'

import Ficha from '../components/Ficha'

export default class HomeScreen extends React.Component {

  state = {
    modalVisible: false,
    mostrar: ""
  };

  static navigationOptions = {
    header: null
  };

  render() {
      return (
          <View contentContainerStyle={styles.hijoContainer}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              selectedItem={this.state.selectedItem}
              onRequestClose={ () => {this.setModalInvisible()} }
            >
              <TouchableOpacity
                onPress={() => {this.setModalInvisible()}}
                style={styles.modalBackIcon}
                >
                <Icon style={styles.modalBackIcon} type='ionicon' name="ios-arrow-back" />
              </TouchableOpacity>
                <Ficha datos={this.state.mostrar} setModalInvisible={() => this.setModalInvisible()} />
            </Modal>
            
            <FlatList
              data={Mock}
              extraData={this.state}
              numColumns={2}
              contentContainerStyle={styles.listaPlana}
              keyExtractor={(item) => item.id}
              renderItem={({item}) =>
              <TouchableHighlight style={styles.listItem} onPress={() => {this.setModalVisible(item)}}>
                  <View style={styles.vistaLista}>
                        <Text style={styles.derecha}>{item.annualLow} €</Text>
                        <Image style={styles.imagenPequeña} source={{ uri: item.smallImageUrl}} />
                        <Text style={styles.titulo} numberOfLines={1}>{item.brand} {item.name}</Text>
                  </View>
               </TouchableHighlight>
              }
              />
          </View>
      )
}

  setModalVisible(item){
    this.setState({ 
      mostrar: item,
      modalVisible: true
    })
  }

  setModalInvisible(){
    this.setState({ 
      modalVisible: false
    })
  }
}

const styles = StyleSheet.create({
  hijoContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 300,
    width: 200,
    padding: 10,
    marginBottom: 40
  },
  listaPlana: {
    paddingTop: 10,
    backgroundColor: '#ffffff'
  },
  imagenPequeña: {
    width: 175,
    height: 175,
    resizeMode: 'contain'
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  titulo: {
    fontSize: 14,
    alignItems: 'center',
    justifyContent: 'center'
  },
  listItem: {
    maxWidth: Dimensions.get('window').width /2,
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'center',
    height: 250
  },
	modalBackIcon: {
    paddingLeft: 30,
    paddingTop: 30,
    alignItems: "flex-start"
	},
  vistaLista: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 250
  },
  derecha: {
    alignSelf: 'flex-end'
  }
});
